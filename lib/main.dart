import 'package:first_app/login.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyLogin());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  var questionIndex = 0;

  void answerQuestion() {
    setState(() {
      questionIndex = questionIndex + 1;
    });
    print(questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    var questions = [
      "what't your favorite color ?",
      "what't your favorite Animal ?"
    ];
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: Column(
          children: [
            Text(questions.elementAt(questionIndex)),
            RaisedButton(
              child: Text('Ans 1'),
              onPressed: answerQuestion,
            ),
            RaisedButton(
              child: Text('Ans 2'),
              onPressed: answerQuestion,
            ),
            RaisedButton(
              child: Text('Ans 3'),
              onPressed: answerQuestion,
            ),
          ],
        ),
      ),
    );
    // TODO: implement build
    throw UnimplementedError();
  }
}
